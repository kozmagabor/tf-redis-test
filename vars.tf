locals {
  num_node_groups_by_env = {
    staging    = 1
    production = 3
  }
  replicas_per_node_group_by_env = {
    staging    = 0
    production = 1
  }
  replicas_per_node_group = var.replicas_per_node_group > 0 ? var.replicas_per_node_group : tonumber(lookup(local.replicas_per_node_group_by_env, terraform.workspace))
  num_node_groups         = var.num_node_groups > 1 ? var.num_node_groups : tonumber(lookup(local.num_node_groups_by_env, terraform.workspace))
}

variable "engine_version" {
  description = "Redis version"
  type        = string
  default     = "6.x"
}

variable "port" {
  description = "Redis port"
  type        = number
  default     = 6379
}

variable "node_type" {
  description = "Redis node type"
  type        = string
  default     = "cache.t2.micro"
}

variable "num_node_groups" {
  type        = number
  description = "umber of node groups (shards) for this Redis replication group. Changing this number will trigger an online resizing operation before other settings modifications"
  default     = 0
}

variable "replicas_per_node_group" {
  type        = number
  description = "Number of replica nodes in each node group. Valid values are 0 to 5. Changing this number will trigger an online resizing operation before other settings modifications."
  default     = -1
}

variable "additional_tags" {
  description = "Additional resource tag(s)"
  type        = map(string)
  default     = {}
}

variable "required_tags" {
  description = "Default resource tags as Object with entries: Name, Owner, Service, Classification"
  type = object({
    Owner          = string
    Service        = string
    Name           = string
    Classification = string
  })

  validation {
    condition     = can(regex("^[[:alnum:]\\+\\-\\=\\.\\_\\:\\/\\@\\ ]+$", var.required_tags.Name)) && length(var.required_tags.Name) > 0 && length(var.required_tags.Name) < 255 && substr(var.required_tags.Name, 0, 4) != "aws:"
    error_message = "Name tag doesn't match for specific constraint, see more: https://docs.aws.amazon.com/AmazonElastiCache/latest/mem-ug/Tagging-Resources.html !"
  }

  validation {
    condition     = can(regex("^[[:alnum:]\\+\\-\\=\\.\\_\\:\\/\\@\\ ]+$", var.required_tags.Service)) && length(var.required_tags.Service) > 0 && length(var.required_tags.Service) < 255 && substr(var.required_tags.Service, 0, 4) != "aws:"
    error_message = "Service tag doesn't match for specific constraint, see more: https://docs.aws.amazon.com/AmazonElastiCache/latest/mem-ug/Tagging-Resources.html !"
  }

  validation {
    condition     = contains(["Data Services", "Platform API", "Infrastructure", "Web"], var.required_tags.Owner)
    error_message = "Owner must be one of the following values: ['Data Services', 'Platform API', 'Infrastructure', 'Web']!"
  }

  validation {
    condition     = contains(["Internal", "Confidential", "Public"], var.required_tags.Classification)
    error_message = "Classification must be one of the following values: ['Internal', 'Confidential', 'Public']!"
  }
}

variable "cluster_name" {
  type = string
}

variable "cluster_description" {
  type = string
}

variable "region" {
  type    = string
  default = "eu-west-1"
  # validation {
  #   condition     = contains(["us-east-2", "eu-west-1", "ap-east-1"], var.region)
  #   error_message = "Region must be one of the following values: ['us-east-2', 'eu-west-1', 'ap-east-1']!"
  # }
}

variable "auth_token" {
  type      = string
  sensitive = true
  description = "Auth token for in transit auth"
}

variable "subnet_group_name" {
  type = string
  default = "default"
  description = "Cache Subnet group"
}