data "aws_vpc" "default" {
  default = true
}

resource "aws_elasticache_parameter_group" "redis_cluster" {
  name   = "custom-redis6-x-cluster-on"
  family = "redis6.x"

  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }
  description = "Customized default parameter group for redis6.x with cluster mode on and change maxmemory-policy to allkeys-lru"
}

resource "aws_elasticache_replication_group" "redis_cluster" {
  replication_group_id          = var.cluster_name
  replication_group_description = var.cluster_description
  node_type                     = var.node_type
  port                          = var.port
  parameter_group_name          = aws_elasticache_parameter_group.redis_cluster.name
  automatic_failover_enabled    = true
  engine_version                = var.engine_version
  auth_token                    = var.auth_token

  at_rest_encryption_enabled = true
  transit_encryption_enabled = true

  subnet_group_name = var.subnet_group_name

  cluster_mode {
    replicas_per_node_group = local.replicas_per_node_group
    num_node_groups         = local.num_node_groups
  }

  tags = merge(
    var.required_tags,
    var.additional_tags
  )
}