cluster_name        = "example-cluster"
cluster_description = "Example Redis cluster"


required_tags = {
  Owner          = "Data Services"
  Classification = "Public"
  Name           = "Example cluster"
  Service        = "Example service"
}

additional_tags = {
  Foo = "bar"
}

# replicas_per_node_group = 1
# num_node_groups         = 2

/* 
 Don't use in production please fill from vault
 or Read from console like this
  ```
  read TF_VAR_auth_token
  export TF_VAR_auth_token
  ```
 or generate with command 
 ```
   export TF_VAR_auth_token=$(openssl rand -hex 16)
 ```
 "auth_token" must contain from 16 to 128 alphanumeric characters or symbols (excluding @, ", and /)
*/

# auth_token = "minimum16chars"
