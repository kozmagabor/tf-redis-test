
## Notes:

Base on What Petra said, I have to finised this excersize in 2 hour, but i cannot.
I known it's not a complete solution, because i have to continoue use this code as Terraform module.

## Weekness:
It's an MVP (minimum viable product)
- Fist of all, it's not completed solution.
- Terraform using local state instead of remote one, and it's not encrypted
- The auth_token for transit encryption isn't encrypted.
- It doesn't handle all possible service configuration option about AWS Elastic Cache services.
    - backups
    - specific KMS id for encryption
    - multi-az support
    - security groups
    - etc
- It doesn't validate additional tags key and value either
- In transit encryption has a few restriction, but it doesn't care of it
  https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/in-transit-encryption.html#in-transit-encryption-enable


## How to use?

### 1. Init local terraform working directory
```
terraform init
```

### 2. Create a "staging" and "production" workspace
```
terraform workspace new staging
terraform workspace new production
```

### 3. Switch to specific environment
Warning: Don't use default workspace
```
terraform workspace select staging
# or
terraform workspace select production
```

### 4. Prepair your config based on cluster/example.conf
Copy example.conf and apply your changes
```
cp cluster/example.conf your-cluster.conf
```

### 5. Set Auth token

Read from stdin
```
read TF_VAR_auth_token
export TF_VAR_auth_token
```
or generate by openssl
```
export TF_VAR_auth_token=$(openssl rand -hex 16)
```

### 6. Check the execution plan: `terraform plan` 
```
terraform plan  -var-file=clusters/example.tfvars
```
### 7. Apply your terraform recipe or actions.
```
terraform apply  -var-file=clusters/example.tfvars
```
### 8. Destroy your stack
```
terraform destroy  -var-file=clusters/example.tfvars
```

